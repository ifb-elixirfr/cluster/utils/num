#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import click
import ldap
import ldap.modlist
import os
import sys
import unicodedata
import re
import random

from envelopes import Envelope
from fabric import Connection
from grampg import PasswordGenerator
from jinja2 import Template as JinjaTemplate
from paramiko.ssh_exception import AuthenticationException
from passlib.hash import sha512_crypt
# from string import Template
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

if os.path.exists('conf.yml'):
    config_path = 'conf.yml'
elif os.path.exists('/etc/num/conf.yml'):
    config_path = '/etc/num/conf.yml'
else:
    click.echo("No config file found")
    sys.exit(1)

config = load(open(config_path, 'r', encoding='utf-8'), Loader=Loader)


@click.group()
@click.version_option()
def num():
    # check if ldap is reachable
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    except ldap.LDAPError as e:
        click.echo("Unable to access ldap server")
        click.echo("Please make sure your VPN connection is enabled")
        click.echo(str(e))
        sys.exit(1)

    ldap_connection.unbind_s()

    # check if cluster login is reachable
    try:
        run_command(config['client_server'], "ls", hide=True)
    except AuthenticationException:
        click.echo("Unable to acces cluster login node")
        click.echo("Please run the following command : ssh-copy-id root@%s" % config['client_server'])
        sys.exit(1)


@num.command()
@click.option('--uid', default=None)
@click.option('--cn', default=None)
@click.option('--ou', default=None)
@click.option('--objc', default=None)
@click.option('--filt', default=None)
def show_ldap(uid, cn, ou, objc, filt):
    """ show ldap info """

    ldap_filter = None
    if uid:
        ldap_filter = '(uid=' + uid + ')'
    if cn:
        ldap_filter = '(cn=' + cn + ')'
    if ou:
        ldap_filter = '(ou:dn:=' + ou + ')'
    if objc:
        ldap_filter = '(objectClass=' + objc + ')'
    if filt:
        ldap_filter = filt

    if ldap_filter:
        click.echo("Search For " + ldap_filter)
        try:
            ldap_connection = ldap.initialize(config['ldap_server'])
            ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
            results = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE, ldap_filter)
            ldap_connection.unbind_s()
            click.echo("Find:")
            click.echo("#" * 80)
            for res in results:
                click.echo(str(res))
                click.echo("#" * 80)
        except ldap.LDAPError as e:
            click.echo(str(e))
            sys.exit(1)


@num.command()
@click.argument('name')
@click.option('--slurm/--no-slurm', default=True)
@click.option('-sq', '--softquota', default=config['project_default_soft_quota'])
def create_project(name, slurm, softquota):
    """ Create a project on the NNCR Cluster infrastructure """

    # cleanup project name (all lower case, no space)
    project = name.replace(' ', '').lower()

    (project_group, ou) = get_project_group_and_ou(project)

    # check if another group exist with same name
    if group_exists(project_group):
        click.echo("Group already used in ldap")
        sys.exit(1)

    # find first available gidNumber
    gid_number = get_next_gid_number()
    if not gid_number:
        click.echo("No more gid number availables !")
        sys.exit(1)

    _auto_add_ou(config['ldap_projects_groups_ou'])
    # create ldap group
    dn = 'cn=%s,%s' % (project_group, ou)
    modlist = {
        "objectClass": ["posixGroup".encode(), "top".encode()],
        "gidNumber": [str(gid_number).encode()]
    }
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        ldap_connection.add_s(dn, ldap.modlist.addModlist(modlist))
    except ldap.LDAPError as e:
        click.echo("Unable to create ldap group for project")
        click.echo(str(e))
        sys.exit(1)

    ldap_connection.unbind_s()

    # create project folder
    # mdt-index with value "-1" means that the folder will be created
    # on the mdt with with the most available inodes, in percentage.
    # it's an easy autobalance solution.
    project_path = get_project_path(name, "project")
    cmd_mkdir_template = config['project_mkdir_template'].format(PATH=project_path)
    run_command(config['client_server'], cmd_mkdir_template)
    run_command(config['client_server'], 'chmod 770 %s' % project_path)

    # set acl on project folder
    _set_acl(project_path, project_group, rights='rwx')

    # set quota on user project folder
    _set_quota('project', project_path, gid_number, softquota)

    # create slurm account for project
    if slurm:
        account_cmd = 'sacctmgr -i add account %s defaultqos=normal qos=normal' % project_group
        run_command(config['client_server'], account_cmd, hide=True, warn=True)

    _run_extra_cmd('project', project_path)


@num.command()
@click.argument('name')
@click.option('-sq', '--softquota', default=config['scratch_default_soft_quota'])
def create_scratch(name, softquota):
    """ Create a scratch on the NNCR Cluster infrastructure """

    # cleanup project name (all lower case, no space)
    project = name.replace(' ', '').lower()

    if not project_group_exists(name):
        click.echo("The linked project %s doesn't exist" % name)
        sys.exit(1)

    (project_group, ou) = get_project_group_and_ou(project)

    # create project folder
    project_path = get_project_path(name, "scratch")
    cmd_mkdir_template = config['scratch_mkdir_template'].format(PATH=project_path)
    run_command(config['client_server'], cmd_mkdir_template)
    run_command(config['client_server'], 'chmod 770 %s' % project_path)

    # set acl on project folder
    _set_acl(project_path, project_group, rights='rwx')

    # set quota on user project folder
    gid_number = _get_group_gid(project)
    _set_quota('scratch', project_path, gid_number, softquota)

    _run_extra_cmd('scratch', project_path)


@num.command()
@click.argument('name')
@click.option('--data/--no-data', default=False)
@click.option('--slurm/--no-slurm', default=True)
def remove_project(name, data, slurm):
    """ remove a project """

    # remove OU
    if project_ou_exists(name):
        subprojects = retrieve_subprojects(name)
        for subproject in subprojects:
            remove_project_group(subproject, name)
            if slurm:
                remove_project_account("%s_%s" % (name, subproject))

        remove_project_ou(name)

    # check if project exists
    if project_group_exists(name):
        remove_project_group(name)
    else:
        click.echo("Unknown project group")
        click.echo("Skipping LDAP management")

    if slurm:
        remove_project_account(name)
    if data:
        remove_project_folder(name)
        remove_project_folder(name, mode="scratch")
    else:
        desactivate_project_folder(name)
        desactivate_project_folder(name, mode="scratch")


@num.command()
@click.argument('name')
@click.option('--slurm/--no-slurm', default=True)
def expire_project(name, slurm):
    """ expire a project """

    desactivate_project_folder(name, mode="project")
    desactivate_project_folder(name, mode="scratch")

    if slurm:
        desactivate_project_account(name)


@num.command()
@click.argument('name')
@click.option('--slurm/--no-slurm', default=True)
def activate_project(name, slurm):
    """ activate a project """

    activate_project_folder(name, mode="project")
    activate_project_folder(name, mode="scratch")

    if slurm:
        activate_project_account(name)


@num.command()
@click.argument('name')
def create_group(name):
    """ Create a group on the NNCR Cluster infrastructure """

    # cleanup project name (all lower case, no space)
    groupname = name.replace(' ', '').lower()
    ou = config['ldap_groups_ou']

    # check if another group exist with same name
    if group_exists(groupname):
        click.echo("Group already used in ldap")
        sys.exit(1)

    # find first available gidNumber
    gid_number = get_next_gid_number()
    if not gid_number:
        click.echo("No more gid number availables !")
        sys.exit(1)

    _auto_add_ou(ou)
    # create ldap group
    dn = 'cn=%s,%s' % (groupname, ou)
    modlist = {
        "objectClass": ["posixGroup".encode(), "top".encode()],
        "gidNumber": [str(gid_number).encode()]
    }
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        ldap_connection.add_s(dn, ldap.modlist.addModlist(modlist))
    except ldap.LDAPError as e:
        click.echo("Unable to create ldap group")
        click.echo(str(e))
        sys.exit(1)

    ldap_connection.unbind_s()


@num.command()
@click.argument('name')
def remove_group(name):
    """ remove a group """

    # cleanup project name (all lower case, no space)
    groupname = name.replace(' ', '').lower()
    ou = config['ldap_groups_ou']

    # check if another group exist with same name
    if not group_exists(groupname):
        click.echo("Group does not exist in ldap")
        sys.exit(1)

    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        results = ldap_connection.search_s(ou, ldap.SCOPE_SUBTREE, 'cn=' + groupname, ['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Unable to delete group")
        click.echo(str(e))
        sys.exit(1)


@num.command()
@click.option('--firstname', prompt='First name of the user', help='First name of the new user.')
@click.option('--lastname', prompt='Last name of the user', help='Last name of the new user.')
@click.option('--email', help='Mail address of the user.')
@click.option('--service', is_flag=True, help='This account is a service account.')
@click.option('--uid_number', default=None)
@click.option('--gid_number', default=None)
@click.option('--home_dir', default=None)
@click.option('--username', default=None)
@click.option('--password', default=None)
@click.option('--notify', is_flag=True)
@click.option('--softquota', default=config['user_default_soft_quota'])
@click.option('--slurm/--no-slurm', default=True)
def create_user(firstname, lastname, email, service, uid_number, gid_number, home_dir, username, password, notify, softquota, slurm):
    """ create a user in ldap directory """

    if not service:
        if not email:
            click.echo("Email is mandatory to create user account")
            sys.exit(1)
        if email_exists(email):
            click.echo("Email already exist: %s" % email)
            sys.exit(1)

    if username is None:
        username = generate_username(firstname, lastname)

    # it may be because it have been provided, or because generate_username loop have failed
    if username_exists(username):
        click.echo("Username already exist: %s" % username)
        sys.exit(1)

    # find next avalaible uid number
    if uid_number is None:
        uid_number = max([get_next_uid_number(), get_next_gid_number()])

    if gid_number is None:
        gid_number = uid_number

    if home_dir is None:
        home_dir = os.path.join(config['user_home'], username)

    # generate password
    if password is None:
        password = (PasswordGenerator().of().between(3, 6, 'letters')
                    .at_least(4, 'numbers')
                    .length(10)
                    .beginning_with('letters')
                    .done()).generate()

    # choose destination ou
    if service:
        suffix = config['ldap_services_ou']
        _auto_add_ou(config['ldap_services_ou'])
    else:
        suffix = config['ldap_people_ou']
        _auto_add_ou(config['ldap_people_ou'])

    suffix_groups = config['ldap_user_groups_ou']
    _auto_add_ou(config['ldap_user_groups_ou'])

    # create user ldap account
    dn = 'uid=' + username + ',' + suffix
    modlist = {
        "objectClass": ["inetOrgPerson".encode(), "posixAccount".encode(), "shadowAccount".encode()],
        "uid": [(str(username)).encode()],
        "sn": [(str(lastname)).encode()],
        "givenName": [(str(firstname)).encode()],
        "cn": [(str(firstname + " " + lastname)).encode()],
        "displayName": [(str(firstname + " " + lastname)).encode()],
        "uidNumber": [(str(uid_number)).encode()],
        "gidNumber": [(str(gid_number)).encode()],
        "loginShell": ["/bin/bash".encode()],
        "homeDirectory": [(str(home_dir)).encode()],
        "userPassword": [(str(generate_password_hash(password))).encode()]
    }
    # add email only if provided
    if email:
        modlist['mail'] = [(str(email)).encode()]

    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        ldap_connection.add_s(dn, ldap.modlist.addModlist(modlist))
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Unable to create ldap user entry")
        click.echo(str(e))
        sys.exit(1)

    if "create_user_group" in config and config['create_user_group']:
        # create user group
        dn = 'cn=' + username + ',' + suffix_groups
        if group_exists(username):
            click.echo("Warning: User group already exist ! ")
        else:
            _auto_add_group(dn, gid_number)
            _ldap_modify_any_group_by_dn(username, dn, ldap.MOD_ADD)

    # add user to hpc group
    if "add_user_to_group_cn" in config and config['add_user_to_group_cn']:
        _auto_add_group(config['group_cn'], get_next_gid_number())
        _ldap_modify_any_group_by_dn(username, config['group_cn'], ldap.MOD_ADD)

    # create homedir
    run_command(config['client_server'], '/sbin/mkhomedir_helper {} 0077'.format(username))
    if "quota_home" in config and config['quota_home']:
        _set_quota('home', home_dir, gid_number, softquota)

    if slurm:
        _slurm_set_default_account_as_demo(username)

    # envoi d'un mail à l'utilisateur
    if notify and email:
        send_email_create_user(email, firstname, username, password)

    _run_extra_cmd('home', home_dir)

    click.echo("%s/%s" % (username, password))


@num.command()
@click.argument('username')
def remove_user(username):
    """ remove a user in ldap directory """
    if not username_exists(username):
        click.echo("Unknown username")
        sys.exit(1)

    # delete user entry in ldap
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        results = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE, 'uid=' + username, ['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Unable to delete user")
        click.echo(str(e))
        sys.exit(1)

    # delete user group entry in ldap
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        results = ldap_connection.search_s(config['ldap_user_groups_ou'], ldap.SCOPE_SUBTREE, 'cn=' + username, ['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Unable to delete user group")
        click.echo(str(e))

    # TODO: use _ldap_modify_any_user_group
    # todo: maybe do it before remove user entry
    # remove user from groups in ldap
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        results = ldap_connection.search_s(config['ldap_groups_ou'], ldap.SCOPE_SUBTREE, 'memberuid=' + username, ['dn', 'memberUid'])
        for dn, entry in results:
            mod_attrs = [(ldap.MOD_DELETE, 'memberUid', [username.encode()])]
            ldap_connection.modify_s(dn, mod_attrs)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Unable to remove user from groups")
        click.echo(str(e))
        sys.exit(1)

    # delete homedir
    home_dir = os.path.join(config['user_home'], username)
    if home_dir and len(home_dir) > len(config['user_home']):
        cmd = 'rm -rf %s' % home_dir
        run_command(config['client_server'], cmd)


@num.command()
@click.argument('username')
@click.argument('project')
@click.argument('partition')
@click.option('-m', '--mins', default=None)
def add_user_to_partition(username, project, partition, mins):
    """ Add an existing user to an existing partition """

    if not username_exists(username):
        click.echo("Unknown username")
        sys.exit(1)

    if not project_group_exists(project):
        click.echo("Unknown project")
        sys.exit(1)

    (project_group, ou) = get_project_group_and_ou(project)

    _slurm_modify_user_partition(username, project_group, partition, "add", mins)


@num.command()
@click.argument('username')
@click.argument('project')
@click.argument('partition')
def remove_user_from_partition(username, project, partition):
    """ Add an existing user to an existing partition """

    if not username_exists(username):
        click.echo("Unknown username")
        sys.exit(1)

    if not project_group_exists(project):
        click.echo("Unknown project")
        sys.exit(1)

    (project_group, ou) = get_project_group_and_ou(project)

    _slurm_modify_user_partition(username, project_group, partition, "remove")


def _slurm_modify_user_partition(username, account, partition, mode="add", mins=None):

    # todo check if account and username exist
    if len(partition) > 0:
        click.echo("slurm " + mode + " " + username + " to " + account + " on " + partition)

        cmd_part = 'sacctmgr -i {mode} user {username} account={account} partitions={partition}'.format(mode=mode,
                                                                                                        username=username,
                                                                                                        account=account,
                                                                                                        partition=partition)

        if mins and mode == "add":
            cmd_part += ' set GrpCPUMins=' + str(mins)

        run_command(config['client_server'],
                    cmd_part,
                    hide=True,
                    warn=True)


@num.command()
@click.option('--slurm/--no-slurm', default=True)
@click.option('--notify', is_flag=True)
@click.argument('username')
@click.argument('project')
def add_user_to_project(slurm, notify, username, project):
    """ Add an existing user to an existing project """

    if not username_exists(username):
        click.echo("Unknown username")
        sys.exit(1)

    if not project_group_exists(project):
        click.echo("Unknown project")
        sys.exit(1)

    _ldap_modify_user_and_project(username, project, ldap.MOD_ADD)

    (project_group, ou) = get_project_group_and_ou(project)
    if slurm:
        # create slurm user for default project
        for partition in config['open_partitions']:
            _slurm_modify_user_partition(username, project_group, partition)

        # disabled as we now set user default account to demo
        # _slurm_set_default_account(username, project_group)

        # remove user if belongs to 'demo' slurm acocunt and update the user default account if it sets as 'demo'
        _slurm_remove_from_demo_project_and_set_first_project_as_default_account(slurm, username, project)

    # envoi d'un mail à l'utilisateur
    if notify:
        send_email_add_user_to_project(username, project_group, project, slurm)


def _slurm_clean_default_account(account):
    if _slurm_check_if_account_exist(account):
        # todo should check if account is not demo
        users_with_defaccount_cmd = 'sacctmgr -n -P list user defaultaccount={} format=user'.format(account)
        users = run_command(config['client_server'], users_with_defaccount_cmd, hide=True, warn=True)
        for user in users.stdout.splitlines():
            _slurm_set_default_account_as_demo(user)


def _slurm_set_default_account_as_demo(username):
    demo_account = 'demo'  # todo maybe add this in config
    demo_partition = config['open_partitions'][0]  # should be fast, todo : add this in config too
    demo_mins = 6 * 60  # 6h
    # check if demo account exist:

    if not _slurm_check_if_account_exist(demo_account):
        # todo find if we should change qos
        create_cmd = "sacctmgr -i add account {account} defaultqos=normal qos=normal".format(account=demo_account)
        run_command(config['client_server'], create_cmd, hide=True, warn=True)

    _slurm_modify_user_partition(username, demo_account, demo_partition, "add", demo_mins)
    _slurm_set_default_account(username, demo_account)


def _slurm_check_if_account_exist(account):
    chk_cmd = "sacctmgr -n -P list account account={account} format=account".format(account=account)

    cmd_res = run_command(config['client_server'], chk_cmd, hide=True, warn=True)
    accounts = cmd_res.stdout.splitlines()

    return len(accounts) > 0


def _slurm_remove_from_demo_project_and_set_first_project_as_default_account(slurm, username, project):
    demo_account = 'demo'
    demo_partition = config['open_partitions'][0]  # should be fast, todo : add this in config too

    # check if user is associated to 'demo' slurm account
    user_accounts_cmd = f"sacctmgr -nP list associations user={username} format=account"
    user_accounts_rslts = run_command(config['client_server'], user_accounts_cmd, hide=True, warn=True)
    user_accounts_list = list(set(user_accounts_rslts.stdout.splitlines()))  # need to clean the accounts list before to use it

    if demo_account in user_accounts_list:

        if slurm:
            # check and update the default slurm account value if it sets to 'demo'
            if _is_user_default_account(username=username, account=demo_account):
                # update the default slurm account value
                click.echo(f"set default account as {project} for {username}")
                updt_dflt_accnt_cmd = f"sacctmgr -i modify user {username} set DefaultAccount={project}"
                run_command(config['client_server'], updt_dflt_accnt_cmd, hide=True, warn=True)

            # remove the user from the 'demo' slurm account and partition
            _slurm_modify_user_partition(username=username, account=demo_account, partition=demo_partition, mode="remove")


@num.command()
@click.argument('username')
@click.argument('account')
def set_user_default_account(username, account):
    """ Add an existing user to an existing partition """
    if _slurm_check_if_account_exist(account) and username_exists(username):
        _slurm_set_default_account(username, account)


def _slurm_set_default_account(username, account):

    click.echo("set default account as " + account + " for " + username)
    run_command(config['client_server'],
                'sacctmgr -i modify user where user={username} set defaultaccount={account}'.format(username=username,
                                                                                                    account=account),
                hide=True,
                warn=True)


def _is_user_default_account(username, account):
    users_with_defaccount_cmd = 'sacctmgr -n -P list user defaultaccount={account} user={username} format=user'.format(username=username,
                                                                                                                       account=account)
    users = run_command(config['client_server'], users_with_defaccount_cmd, hide=True, warn=True)
    return len(users.stdout.splitlines()) > 0


@num.command()
@click.option('--slurm/--no-slurm', default=True)
@click.argument('username')
@click.argument('project')
def remove_user_from_project(slurm, username, project):
    """ Remove an existing user from an existing project """
    _remove_user_from_project(slurm, username, project)


def _remove_user_from_project(slurm, username, project):
    # We create this function as it can be used by another function
    (account, ou) = get_project_group_and_ou(project)
    _ldap_modify_user_and_project(username, project, ldap.MOD_DELETE)
    if slurm:
        if _is_user_default_account(username, account):
            _slurm_set_default_account_as_demo(username)

        if _slurm_check_if_account_exist(account):
            cmd_account_partition = 'sacctmgr -nP list associations account={account} format=partition'.format(account=account)
            parts = run_command(config['client_server'], cmd_account_partition, hide=True, warn=True)
            for partition in list(set(parts.stdout.splitlines())):
                _slurm_modify_user_partition(username, account, partition, 'remove')


@num.command()
@click.argument('username')
@click.argument('group')
def add_user_to_group(username, group):
    """ Add an existing user to an existing group """
    _ldap_modify_user_and_group(username, group, ldap.MOD_ADD)


@num.command()
@click.argument('username')
@click.argument('group')
def remove_user_from_group(username, group):
    """ Remove an existing user from an existing group """
    _ldap_modify_user_and_group(username, group, ldap.MOD_DELETE)


def _ldap_modify_user_and_project(username, project, ldap_mod):
    if not username_exists(username):
        click.echo("Unknown username")
        sys.exit(1)

    if not project_group_exists(project):
        click.echo("Unknown project")
        sys.exit(1)

    (project_group, ou) = get_project_group_and_ou(project)

    return _ldap_modify_any_user_group(username, project_group, ou, ldap_mod)


def _ldap_modify_user_and_group(username, group, ldap_mod):
    if not username_exists(username):
        click.echo("Unknown username")
        sys.exit(1)

    if not group_exists(group):
        click.echo("Unknown group")
        sys.exit(1)

    (group_dn, group_entry) = _get_ldap_group(group)[0]

    return _ldap_modify_any_group_by_dn(username, group_dn, ldap_mod)


def _ldap_modify_any_user_group(username, group, ou, ldap_mod):
    #  ldap_mod can be ldap.MOD_DEL or ldap.MOD_ADD

    group_dn = 'CN={group},{ou}'.format(group=group, ou=ou)
    _ldap_modify_any_group_by_dn(username, group_dn, ldap_mod)

    return group


def _ldap_modify_any_group_by_dn(username, dn, ldap_mod):
    #  ldap_mod can be ldap.MOD_DEL or ldap.MOD_ADD

    try:
        # modify ldap group for user
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        mod_attrs = [(ldap_mod, 'memberUid', [username.encode()])]
        ldap_connection.modify_s(dn, mod_attrs)
    except ldap.LDAPError as e:
        click.echo("Unable to modify ldap group for user")
        click.echo(str(e))
        sys.exit(1)


@num.command()
@click.option('--readwrite/--readonly', default=True)
@click.option('--scratch', default=None)
@click.argument('group')
@click.argument('project')
def add_group_to_project(readwrite, group, project, scratch):
    """ Add an existing group to an existing project """

    mode = 'project' if not scratch else 'scratch'

    if not group_exists(group):
        click.echo("Unknown group")
        sys.exit(1)

    if not project_group_exists(project):
        click.echo("Unknown project")
        sys.exit(1)

    if readwrite:
        rights = 'rwx'
    else:
        rights = 'rx'

    project_path = get_project_path(project, mode)

    acl_options = '-R'
    _set_acl(project_path, group, acl_options, remove=True)
    _set_acl(project_path, group, acl_options, rights=rights)


@num.command()
@click.option('--mode', type=click.Choice(['home', 'project', 'scratch']), default="project")
@click.argument('path')
@click.option('-sq', '--softquota', required=True)
@click.option('-iq', '--inodequota', default=None)
def set_quota(mode, path, softquota, inodequota):
    """ Set a quota to a project or home directory"""
    groupname = os.path.basename(path)
    gid_number = _get_group_gid(groupname)
    _set_quota(mode, path, gid_number, softquota, inodequota)


def _set_quota(mode, path, gid, softquota, inodequota=None):
    """ Set a quota to a project or home directory"""
    name = os.path.basename(path)
    matches = re.match(r'^(.*\d)\s*([a-zA-Z]*)$', softquota)
    size_str, unit_str = matches.groups()
    hardquota = str(round(int(size_str) * config['default_hard_quota_factor'])) + unit_str
    # todo: should manage the factor when quota is passed in T To G or Go ...
    # should work well for Go or G
    inodesoft = round(int(size_str) * config['default_inode_factor'])
    if inodequota:
        inodesoft = int(inodequota)

    inodehard = round(inodesoft * config['default_hard_quota_factor'])

    # todo check if it work if there are not all var in string template
    cmd = config[mode + '_quota_template'].format(NAME=name,
                                                  PATH=path,
                                                  GIDNUMBER=gid,
                                                  SOFTQUOTA=softquota,
                                                  HARDQUOTA=hardquota,
                                                  INODESOFTLIMIT=inodesoft,
                                                  INODEHARDLIMIT=inodehard)

    run_command(config['client_server'], cmd, hide=True)


@num.command()
@click.argument('path')
def set_acl(path):
    """ Set a acl to a project or home directory"""
    groupname = os.path.basename(path)

    acl_options = '-R'
    # todo should remove all acl (not only group one)
    # with setfacl -R -bn /shared/projects/<groupname>
    _set_acl(path, groupname, acl_options, remove=True)
    _set_acl(path, groupname, acl_options, rights='rwx')


def _set_acl(path, group, acl_options='', rights='', remove=False):
    """ Set a acl to a project or home directory"""

    if "setfacl_deactivate" not in config or not config['setfacl_deactivate']:

        groupname = os.path.basename(path)
        gid_number = _get_group_gid(groupname)

        mode = 'setfacl_project_template'
        if "setfacl_right_uppercase" in config and config['setfacl_right_uppercase']:
            rights = rights.upper()

        if remove:
            mode = 'setfacl_project_remove_template'

        cmd = config[mode].format(PATH=path,
                                  GROUP=group,
                                  GIDNUMBER=gid_number,
                                  OPTIONS=acl_options,
                                  RIGHTS=rights)
        run_command(config['client_server'], cmd, hide=True)


def remote_path_exists(path):
    cmd = 'ls %s' % path
    result = run_command(config['client_server'], cmd, hide=True, warn=True)
    return result.ok


def run_command(server, command, hide=False, warn=False):
    client = Connection(server, user='root', connect_timeout=10)
    if server == 'localhost':
        res = client.local(command, hide=hide, warn=warn, pty=True)
    else:
        res = client.run(command, hide=hide, warn=warn, pty=True, in_stream=False)
    client.close()
    return res


def get_group_and_ou(name):
    ou = config['ldap_groups_ou']
    group = name

    return (group, ou)


def get_project_group_and_ou(name):
    ou = config['ldap_projects_groups_ou']
    project_group = name

    return (project_group, ou)


def get_project_path(name, mode="project", expired=False):
    trash_dir = '' if not expired else '.trash'
    if (mode + '_home') in config.keys():
        home = config[mode + '_home']
        project_path = os.path.join(home, trash_dir, name)
        if len(project_path) > len(home):
            return project_path
        else:
            return ""
    else:
        return ""


def project_group_exists(name):
    """ Check if a project group with the given already exists """
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])

    (project_group, ou) = get_project_group_and_ou(name)

    res = ldap_connection.search_s(base=ou,
                                   scope=ldap.SCOPE_SUBTREE,
                                   filterstr='(&(objectClass=posixGroup)(cn=%s))' % project_group)
    ldap_connection.unbind_s()
    return len(res) > 0


def retrieve_subprojects(name):
    subprojects = []
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    base = "ou=%s,%s" % (name, config['ldap_projects_groups_ou'])
    results = ldap_connection.search_s(base=base,
                                       scope=ldap.SCOPE_ONELEVEL,
                                       filterstr='(objectClass=posixGroup)',
                                       attrlist=['cn'])
    ldap_connection.unbind_s()
    for result in results:
        subproject_group = result[1].get('cn')[0].decode('utf-8')
        subproject = subproject_group[len(name) + 1:]
        subprojects.append(subproject)

    return subprojects


def _auto_add_group(dn, gid_number):
    _auto_add_ou(dn)
    (attr, val) = dn.split(',')[0].split('=')
    if attr == 'cn':
        if not group_exists(val):
            modlist = {
                "objectClass": ["posixGroup".encode(), "top".encode()],
                "gidNumber": [(str(gid_number)).encode()]
            }
            try:
                ldap_connection = ldap.initialize(config['ldap_server'])
                ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
                ldap_connection.add_s(dn, ldap.modlist.addModlist(modlist))
                ldap_connection.unbind_s()
            except ldap.LDAPError as e:
                click.echo("Unable to create ldap group entry " + dn)
                click.echo(str(e))
                sys.exit(1)


def _auto_add_ou(dn):
    spl_dn = dn.split(',')
    len_spl = len(spl_dn)
    len_cpt = len_spl
    # we should start from the end of the dn
    while len_cpt >= 0:
        len_cpt -= 1
        (attr, val) = spl_dn[len_cpt].split('=')
        if attr == 'ou':
            ou_to_add = ','.join(spl_dn[len_cpt:len_spl])
            _add_ou(ou_to_add)


def _add_ou(dn):
    (attr, val) = dn.split(',')[0].split('=')
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    if attr == 'ou':
        # check if ou already exist
        try:
            res = ldap_connection.search_s(base=dn, scope=ldap.SCOPE_BASE)
        except ldap.LDAPError:
            res = []

        if len(res) == 0:
            click.echo("add ou: " + str(val))
            # add ou if no res
            modlist = {
                "objectClass": ["organizationalUnit".encode(), "top".encode()],
                "ou": [val.encode()]
            }
            try:
                ldap_connection.add_s(dn, ldap.modlist.addModlist(modlist))
            except ldap.LDAPError as e:
                click.echo("Unable to create ldap ou for dn" + dn)
                click.echo(str(e))
                sys.exit(1)
    else:
        click.echo(dn + "is not an ou")

    ldap_connection.unbind_s()


def project_ou_exists(name):
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    res = ldap_connection.search_s(base=config['ldap_projects_groups_ou'],
                                   scope=ldap.SCOPE_ONELEVEL,
                                   filterstr='(&(objectClass=organizationalUnit)(ou=%s))' % name)
    ldap_connection.unbind_s()
    return len(res) > 0


def create_project_ou(name):
    dn = 'ou=%s,%s' % (name, config['ldap_projects_groups_ou'])
    _auto_add_ou(dn)


def remove_project_ou(name):

    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        results = ldap_connection.search_s(config['ldap_projects_groups_ou'], ldap.SCOPE_ONELEVEL, '(&(objectClass=organizationalUnit)(ou=%s))' % name, ['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Unable to delete project ou")
        click.echo(str(e))
        sys.exit(1)


def _get_ldap_group(name):
    res = []
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        res = ldap_connection.search_s(base=config['ldap_base_dn'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='(&(objectClass=posixGroup)(cn=%s))' % name)
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo(str(e))
        res = []

    return res


def _get_group_gid(name):
    ldap_group = _get_ldap_group(name)

    if len(ldap_group) > 0:
        dn, entry = ldap_group[0]  # todo check if only one result
        gid_number = entry['gidNumber'][0].decode()
    else:
        click.echo(str(name) + "Not found in ldap")
        sys.exit(1)
    return gid_number


# todo : factoryze with project
def group_exists(name):
    """ Check if a project group with the given already exists """
    res = _get_ldap_group(name)
    return len(res) > 0


def remove_project_group(name):
    ou = config['ldap_projects_groups_ou']

    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        results = ldap_connection.search_s(ou, ldap.SCOPE_SUBTREE, 'cn=' + name, ['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Unable to delete project group")
        click.echo(str(e))
        sys.exit(1)


def activate_project_account(name):

    # activate slurm account for project
    account_cmd = 'sacctmgr -i update account %s set maxjobs=-1' % name
    result = run_command(config['client_server'], account_cmd, hide=True, warn=True)
    if not result.ok and "Nothing modified" not in result.stdout:
        click.echo("Failed to activate account %s" % name)


def desactivate_project_account(name):

    # desactivate slurm account for project
    account_cmd = 'sacctmgr -i update account %s set maxjobs=0' % name
    result = run_command(config['client_server'], account_cmd, hide=True, warn=True)
    if not result.ok and "Nothing modified" not in result.stdout:
        click.echo("Failed to desactivate account %s" % name)


def remove_project_account(name):

    _slurm_clean_default_account(name)

    # delete slurm account for project
    account_cmd = 'sacctmgr -Q -i delete account %s' % name
    result = run_command(config['client_server'], account_cmd, hide=True, warn=True)
    if not result.ok and "Nothing deleted" not in result.stdout:
        click.echo("Failed to delete account %s" % name)


def activate_project_folder(name, mode='project'):
    project_dir = get_project_path(name, mode)
    project_dir_expired = get_project_path(name, mode, expired=True)

    if remote_path_exists(project_dir) and remote_path_exists(project_dir_expired):
        click.echo("Error: two projects with the same name %s and %s" % (project_dir, project_dir_expired))
        sys.exit(1)

    if project_dir and remote_path_exists(project_dir_expired):
        cmd = 'mv %s %s' % (project_dir_expired, project_dir)
        run_command(config['client_server'], cmd)


def desactivate_project_folder(name, mode='project'):
    project_dir = get_project_path(name, mode)
    project_dir_expired = get_project_path(name, mode, expired=True)

    if remote_path_exists(project_dir) and remote_path_exists(project_dir_expired):
        click.echo("Error: two projects with the same name %s and %s" % (project_dir, project_dir_expired))
        sys.exit(1)

    if project_dir_expired and remote_path_exists(project_dir):
        cmd = 'mv %s %s' % (project_dir, project_dir_expired)
        run_command(config['client_server'], cmd)


def remove_project_folder(name, mode='project'):
    project_dir = get_project_path(name, mode)
    project_dir_expired = get_project_path(name, mode, expired=True)

    if os.path.exists(project_dir_expired):
        rm_cmd = 'rm -rf %s' % project_dir_expired
        run_command(config['client_server'], rm_cmd)

    if os.path.exists(project_dir):
        rm_cmd = 'rm -rf %s' % project_dir
        run_command(config['client_server'], rm_cmd)


def get_next_gid_number():
    """ get the next free uid in LDAP """
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    results = ldap_connection.search_s(base=config['ldap_groups_ou'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='(objectclass=posixGroup)',
                                       attrlist=['gidNumber'])
    ldap_connection.unbind_s()

    if len(results) == 0:
        next = 10000
    else:
        gids = sorted([int(result[1].get('gidNumber')[0]) for result in results])
        next = gids[-1] + 1

    return next


def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')


def generate_username(firstname, lastname, cpt_firstname=1, cpt_lastname=0):
    """ generate username first letter of firstname and lastname """
    lastname2 = strip_accents(lastname).lower().replace(" ", "").replace("-", "").replace("'", "")
    firstname2 = strip_accents(firstname).lower().replace(" ", "").replace("-", "").replace("'", "")
    username = firstname2[:cpt_firstname] + lastname2[0:len(lastname2) - cpt_lastname]  # we need explicit len as 0:-0 return empty string

    if username_exists(username) and len(firstname2) >= cpt_firstname:
        username = generate_username(firstname2, lastname2, cpt_firstname + 1, cpt_lastname)

    if username_exists(username) and len(lastname2) > cpt_lastname + 1:
        username = generate_username(firstname2, lastname2, 1, cpt_lastname + 1)

    if username_exists(username):
        username = generate_username(firstname2, lastname2 + str(random.randint(1, 42)))

    return username


def username_exists(username):
    """ check if username already exists """
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    results = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE, 'uid=' + username, ['uidNumber'])
    ldap_connection.unbind_s()
    return len(results) > 0


def email_exists(email):
    """ check if email already exists """
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    results = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE, 'mail=' + email, ['uidNumber'])
    ldap_connection.unbind_s()
    return len(results) > 0


def get_user(username):
    """ retrieve user's firstname from ldap """
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    results = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE, 'uid=' + username, ['givenName', 'sn', 'mail'])
    ldap_connection.unbind_s()
    if len(results) > 0:
        return {'firstname': results[0][1].get('givenName')[0].decode('utf-8'),
                'lastname': results[0][1].get('sn')[0].decode('utf-8'),
                'email': results[0][1].get('mail')[0].decode('utf-8')}
    else:
        return None


def get_next_uid_number():
    """ get the next free uid in LDAP """
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    results = ldap_connection.search_s(config['ldap_people_ou'],
                                       ldap.SCOPE_SUBTREE,
                                       'objectclass=posixaccount',
                                       ['uidNumber'])
    ldap_connection.unbind_s()

    if len(results) == 0:
        return int(config['min_uid_number'])
    else:
        uids = sorted([int(result[1].get('uidNumber')[0]) for result in results])
        return uids[-1] + 1


def _run_extra_cmd(type, path):
    """
    Run an optional extra script in the home, project or scratch directory
    type in ['home', 'project', 'scratch']
    """
    if (type + '_extra_cmd_template') in config and config[type + '_extra_cmd_template']:
        cmd = config[type + '_extra_cmd_template'].format(PATH=path)
        run_command(config['client_server'], cmd, hide=True)


def send_email_create_user(email, firstname, username, password):
    body = u"""
Bonjour {{firstname}},

Voici votre compte utilisateur pour {{cluster_name_fr}}.
{{cluster_homepage}}

Identifiant : {{username}}
Mot de passe : {{password}}

Ce compte a une durée de validité de 1 an. Au-delà, une procédure de renouvellement devra être enclenchée.

Vous ne devez en aucun cas communiquer votre mot de passe.

L'utilisation de ce compte vous engage à respecter la charte d'utilisation des infrastructures fédérées par l'Institut Français de Bioinformatique (IFB) : http://goo.gl/HYQP47


Accès au cluster SLURM :
ssh {{username}}@{{login_node_address}}

{{other_services_fr}}


Pour en savoir plus sur les infrastructures de calcul de l'IFB: https://www.france-bioinformatique.fr/clusters-ifb/

***

Hello {{firstname}},

Here is your user account for {{cluster_name_en}}.
{{cluster_homepage}}

Username : {{username}}
Password : {{password}}

This account has a validity of 1 year. Beyond that, a renewal procedure will have to be triggered.

You must not communicate your password.

The use of this account commits you to respect the charter of use of infrastructures federated by the French Institute of Bioinformatics (IFB) : http://goo.gl/HYQP47


Access to the SLURM cluster:
ssh {{username}}@{{login_node_address}}

{{other_services_en}}


To learn more about IFB's computing infrastructure: https://www.france-bioinformatique.fr/clusters-ifb/
"""
    template = JinjaTemplate(body)
    envelope = Envelope(
        from_addr=(config['notification_sender'], 'IFB Core Cluster'),
        to_addr=(email),
        subject="Votre compte utilisateur pour l'infrastructure de calcul de l'IFB",
        text_body=template.render(firstname=firstname,
                                  username=username,
                                  password=password,
                                  cluster_name_fr=config['email_cluster_name_fr'],
                                  cluster_name_en=config['email_cluster_name_en'],
                                  cluster_homepage=config['email_cluster_homepage'],
                                  login_node_address=config['email_login_node_address'],
                                  other_services_fr=config['email_other_services_fr'],
                                  other_services_en=config['email_other_services_en'])
    )

    # Send the envelope using an ad-hoc connection...
    envelope.send(config['smtp_host'],
                  login=config.get('smtp_login', None),
                  password=config.get('smtp_password', None),
                  tls=config.get('smtp_usetls', False))


def send_email_add_user_to_project(username, project_group, project, slurm):
    body = u"""
Bonjour {{firstname}},

Vous disposez à présent d'un accès à l'espace projet {{project}} sur {{cluster_name_fr}}.
{{cluster_homepage}}

Vous pouvez stocker vos données liés à ce projet dans le dossier {{project_path}}.

{% if scratch_path != '' %}
Pour des calculs intensifs générants beaucoup d’entrées et sorties temporaires, vous avez peut-être à disposition un espace scratch. Merci d’en faire usage pour soulager l’infrastructure de stockage.: {{scratch_path}}
Vous pouvez en demander un si ce n'est pas le cas.
{% endif %}

{% if slurm %}
Vous pouvez lancer vos traitements SLURM liés à ce projet en utilisant l'account {{project_group}} (option -A de srun / sbatch).
{% endif %}

Pour en savoir plus sur les infrastructures de calcul de l'IFB: https://www.france-bioinformatique.fr/clusters-ifb/

***

Hello {{firstname}},

You now have access to the {{project}} project space on {{cluster_name_en}}.
{{cluster_homepage}}

You can store your data related to this project in the {{project_path}} folder.

{% if scratch_path != '' %}
For intensive calculations generating a lot of temporary inputs and outputs, you may have a scratch space available. Please make use of it to relieve the storage infrastructure: {{scratch_path}}
You can request one if it isn't the case.
{% endif %}

{% if slurm %}
You can start your SLURM jobs related to this project by using the account {{project_group}} (option -A from srun / sbatch).
{% endif %}

To learn more about IFB's computing infrastructure: https://www.france-bioinformatique.fr/clusters-ifb/
"""
    user = get_user(username)
    template = JinjaTemplate(body)
    envelope = Envelope(
        from_addr=(config['notification_sender'], 'IFB Core Cluster'),
        to_addr=(user['email']),
        subject="Accès au projet {} sur l'infrastructure de calcul de l'IFB".format(project_group),
        text_body=template.render(firstname=user['firstname'],
                                  project=project,
                                  project_path=get_project_path(project),
                                  scratch_path=get_project_path(project, "scratch"),
                                  project_group=project_group,
                                  slurm=slurm,
                                  cluster_name_fr=config['email_cluster_name_fr'],
                                  cluster_name_en=config['email_cluster_name_en'],
                                  cluster_homepage=config['email_cluster_homepage'])
    )

    # Send the envelope using an ad-hoc connection...
    envelope.send(config['smtp_host'],
                  login=config.get('smtp_login', None),
                  password=config.get('smtp_password', None),
                  tls=config.get('smtp_usetls', False))


def generate_password_hash(password):
    return '{CRYPT}' + sha512_crypt.hash(password)
