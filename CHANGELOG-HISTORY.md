<a name="unreleased"></a>
## [Unreleased]


<a name="0.2.0"></a>
## [0.2.0] - 2019-10-03
### Add
- Add LICENSE
- Add some log on ldap error

### Add
- add a changelog

### Allow
- allow namesake in ldap

### Update
- update config example

### Use
- use uid only in user dn


<a name="0.1.9"></a>
## [0.1.9] - 2019-09-11
### Fix
- fix bug that put all new user in the service ou

### Increase
- increase maximum complexity for pylama :)


<a name="0.1.8"></a>
## [0.1.8] - 2019-09-06
### Add
- add --username option for account creation + fix infinite loop risk if generate username
- add option for uid and gid number + home directory

### Update
- update num version


<a name="0.1.7"></a>
## [0.1.7] - 2019-08-23
### Add
- add in_stream option for remote command
- add a timeout on ssh connection
- add ci job to run pylama during merge requests
- add more variable in config

### Allow
- allow num to be run in cron (without tty)

### Check
- check if email already exist before create user

### Complete
- complete sample config

### Explicit
- Explicit close client connection as recommanded by paramiko faq

### Make
- make comment clearer

### Move
- move config loading to the top of the module to make it possible to use config params in click options declaration

### Update
- update num version

### Upgrade
- upgrade fabric dependencie

### Use
- use config for bind_dn and base_dn in email_exists function

### Merge Requests
- Merge branch 'remove_project' into 'master'


<a name="0.1.6"></a>
## [0.1.6] - 2019-07-25
### Add
- add better handling of users default account update when removing a project
- add condition for quota and parent folder
- add quota for user and project

### Merge Requests
- Merge branch 'cronjobs' into 'master'


<a name="0.1.3"></a>
## [0.1.3] - 2019-03-27
### Ajout
- Ajout option notification email lors de la creation d'un compte

### Correction
- correction du mail et ajout de la création du homedir via mkhomedire_helper

### Merge
- Merge branch 'master' into mail

### Mise
- mise à jour du mail de bienvenue

### Modify
- modify create_user/create_homedir to create home folders with drwx------ permissions

### Reorganisation
- reorganisation du dépôt

### Merge Requests
- Merge branch 'mail' into 'master'
- Merge branch 'mail' into 'master'


<a name="0.1.1"></a>
## [0.1.1] - 2019-01-18
### Add
- Add detailed commands to push ssh key to login nodes
- Add command create-user and add-user-to-project

### Add
- add check on groups for project creation
- add remove-project
- add remove-user function: remove user from ldap + groups

### Added
- added test to check if fabric client is allowed
- added script and data for m1 account creation

### Ajout
- Ajout d'un fichier de configuration yml Prise en charge d'une execution en local sur le login node (pas besoin d'accès ssh dans ce cas)
- Ajout de la commande add-group-to-project Implements [#6](https://gitlab.com/ifb-elixirfr/cluster/num/issues/6)
- Ajout de l'option --slurm/--no-slurm à la commande add-user-to-project Implements [#5](https://gitlab.com/ifb-elixirfr/cluster/num/issues/5)
- Ajout de l'option `-p` à la commande add-user-to-project pour cibler un sous-projet Implement [#4](https://gitlab.com/ifb-elixirfr/cluster/num/issues/4)
- Ajout de la commande create-project
- Ajout de la dependance vers la lib ldap
- Ajout gitignore
- Ajout de la config pylama

### Ajout
- ajout création/suppression du homedir unix lors de la création/suppresion d'un utilisateur
- ajout d'un test pour l'accès au serveur ldap
- ajout automatique de l utilisateur au groupe hpc

### Changed
- changed crypt lib to be compatible with mac environment

### Correction
- correction du message d'erreur si connexion impossible
- correction du message d'erreur si connexion impossible
- correction prise en compte accents sur prenom
- correction generation username: apostrophe dans le nom et accents
- correction mot de passe

### Fix
- fix de l'adresse du login node de prod

### Fixed
- fixed pep8/pylama validation
- fixed pep8/pylama validation

### Generation
- generation automatique du mot de passe

### Integration
- Integration de fabric et finalisation de la commande create-project

### Merge
- Merge branch 'master' of 192.168.103.4:taskforce-nncr/num

### Modification
- modification projets -> projects

### Num
- num for production

### Prise
- prise en charge complète des sous-projets et des données dans la commande remove-project

### Rangement
- rangement des scripts specifiques aux formations

### Refactoring
- Refactoring de la commande create-project pour la prise en charge complète des sous-projets Implement [#3](https://gitlab.com/ifb-elixirfr/cluster/num/issues/3)

### Remove
- remove authroization for jacques to all student projects folders

### Renommage
- Renommage projects en projets

### Simplification
- Simplification de la commande create-user pour ne pas avoir besoin d'un default project

### Suppression
- suppression des acls du projet parent pour un sous-projet
- suppression d un print inutile

### Update
- Update README.md

### Merge Requests
- Merge branch 'config' into 'master'
- Merge branch 'subproject' into 'master'
- Merge branch 'check_ldap' into 'master'
- Merge branch 'prod' into 'master'
- Merge branch 'prod' into 'master'
- Merge branch 'cleanup' into 'master'
- Merge branch 'crypt' into 'master'
- Merge branch 'crypt' into 'master'


<a name="0.1.0"></a>
## 0.1.0 - 2018-09-26
### Refactoring
- Refactoring * Mise en place d'un setup.py * Renommage de l'outil en num (NNCR cluster User Manager) * Documentation de la procédure d'installation en mode développement * Création d'un groupe click


[Unreleased]: https://gitlab.com/ifb-elixirfr/cluster/num/compare/0.2.0...HEAD
[0.2.0]: https://gitlab.com/ifb-elixirfr/cluster/num/compare/0.1.9...0.2.0
[0.1.9]: https://gitlab.com/ifb-elixirfr/cluster/num/compare/0.1.8...0.1.9
[0.1.8]: https://gitlab.com/ifb-elixirfr/cluster/num/compare/0.1.7...0.1.8
[0.1.7]: https://gitlab.com/ifb-elixirfr/cluster/num/compare/0.1.6...0.1.7
[0.1.6]: https://gitlab.com/ifb-elixirfr/cluster/num/compare/0.1.3...0.1.6
[0.1.3]: https://gitlab.com/ifb-elixirfr/cluster/num/compare/0.1.1...0.1.3
[0.1.1]: https://gitlab.com/ifb-elixirfr/cluster/num/compare/0.1.0...0.1.1
